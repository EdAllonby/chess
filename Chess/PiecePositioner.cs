﻿using System.Collections.Generic;
using System.Linq;

namespace Chess
{
    public class PiecePositioner
    {
        private readonly Board board;

        public PiecePositioner(Board board)
        {
            this.board = board;
        }

        public void SetupStandardBoard()
        {
            SetupStandardPieces(1);
            SetupStandardPieces(8, false);
            SetupStandardPawns(2);
            SetupStandardPawns(7, false);
        }

        public void SetupStandardPieces(int row, bool isFirstPlayerPiece = true)
        {
            board.AddPiece(new Rook(isFirstPlayerPiece), new BoardCoordinate(1, row));
            board.AddPiece(new Rook(isFirstPlayerPiece), new BoardCoordinate(8, row));

            board.AddPiece(new Knight(isFirstPlayerPiece), new BoardCoordinate(2, row));
            board.AddPiece(new Knight(isFirstPlayerPiece), new BoardCoordinate(7, row));

            board.AddPiece(new Bishop(isFirstPlayerPiece), new BoardCoordinate(3, row));
            board.AddPiece(new Bishop(isFirstPlayerPiece), new BoardCoordinate(6, row));

            board.AddPiece(new Queen(isFirstPlayerPiece), new BoardCoordinate(4, row));
            board.AddPiece(new King(isFirstPlayerPiece), new BoardCoordinate(5, row));
        }

        public void SetupStandardPawns(int row, bool isFirstPlayerPiece = true)
        {
            List<int> xCoordinates = Enumerable.Range(1, 8).ToList();

            xCoordinates.ForEach(xCoordinate => board.AddPiece(new Pawn(isFirstPlayerPiece), new BoardCoordinate(xCoordinate, row)));
        }
    }
}
