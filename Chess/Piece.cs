﻿using System.Collections.Generic;
using System.Linq;

namespace Chess
{
    public abstract class Piece
    {
        protected Piece(bool isFirstPlayerPiece = true)
        {
            IsFirstPlayerPiece = isFirstPlayerPiece;
        }

        public bool IsFirstPlayerPiece { get; private set; }

        public bool HasMoved { get; set; }

        public abstract IEnumerable<BoardCoordinate> GetMovesFrom(BoardCoordinate startingLocation, int boardSize = Board.DefaultBoardSize);

        public virtual bool IsCaptureAllowed(BoardCoordinate origin, BoardCoordinate destination)
        {
            return true;
        }

        public virtual bool IsNonCaptureAllowed(BoardCoordinate origin, BoardCoordinate destination)
        {
            return true;
        }

        protected static IEnumerable<BoardCoordinate> GetAllRadialMovesFrom(BoardCoordinate startingLocation, int distance)
        {
            return GetRadialDiagonalFromInclusive(startingLocation, distance).Union(GetRadialAdjacentFromInclusive(startingLocation, distance));
        }

        protected static IEnumerable<BoardCoordinate> GetRadialDiagonalFrom(BoardCoordinate startingPosition, int distance)
        {
            yield return new BoardCoordinate(startingPosition.X + distance, startingPosition.Y + distance);
            yield return new BoardCoordinate(startingPosition.X + distance, startingPosition.Y - distance);
            yield return new BoardCoordinate(startingPosition.X - distance, startingPosition.Y + distance);
            yield return new BoardCoordinate(startingPosition.X - distance, startingPosition.Y - distance);
        }

        private static IEnumerable<BoardCoordinate> GetRadialAdjacentFrom(BoardCoordinate startingPosition, int distance)
        {
            yield return new BoardCoordinate(startingPosition.X + distance, startingPosition.Y);
            yield return new BoardCoordinate(startingPosition.X - distance, startingPosition.Y);
            yield return new BoardCoordinate(startingPosition.X, startingPosition.Y + distance);
            yield return new BoardCoordinate(startingPosition.X, startingPosition.Y - distance);
        }

        private static IEnumerable<BoardCoordinate> GetRadialDiagonalFromInclusive(BoardCoordinate startingPosition, int distance)
        {
            IEnumerable<int> squaresToRadiate = Enumerable.Range(1, distance);
            return squaresToRadiate.SelectMany(square => GetRadialDiagonalFrom(startingPosition, square));
        }

        private static IEnumerable<BoardCoordinate> GetRadialAdjacentFromInclusive(BoardCoordinate startingPosition, int distance)
        {
            IEnumerable<int> squaresToRadiate = Enumerable.Range(1, distance);
            return squaresToRadiate.SelectMany(square => GetRadialAdjacentFrom(startingPosition, square));
        }
    }
}