﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Chess
{
    public class Board
    {
        public const int DefaultBoardSize = 8;
        private readonly int boardSize;
        private readonly Piece[,] pieces;

        public Board(int boardSize = DefaultBoardSize)
        {
            if (boardSize <= 0)
            {
                throw new ArgumentException("boardSize");
            }

            this.boardSize = boardSize;

            pieces = new Piece[boardSize, boardSize];
        }

        public int PieceCount
        {
            get { return pieces.Cast<Piece>().Count(p => p != null); }
        }

        public void AddPiece(Piece piece, BoardCoordinate moveTarget)
        {
            if (piece == null)
            {
                throw new ArgumentNullException("piece");
            }

            if (!moveTarget.IsCoordinateValidForBoardSize(boardSize))
            {
                throw new ArgumentException("moveTarget");
            }

            SetPiece(piece, moveTarget);
        }

        public void MovePiece(BoardCoordinate origin, BoardCoordinate destination)
        {
            VerifyCoordinatesOrThrow(origin, destination);

            Piece pieceToMove = GetPiece(origin);
            AddPiece(pieceToMove, destination);
            RemovePiece(origin);
            pieceToMove.HasMoved = true;
        }

        public void RemovePiece(BoardCoordinate coordinateForRemoval)
        {
            if (!DoesPieceExistAt(coordinateForRemoval))
            {
                throw new ArgumentException("coordinateForRemoval");
            }

            SetPiece(null, coordinateForRemoval);
        }

        public Piece GetPiece(BoardCoordinate coordinateToRetrieve)
        {
            return pieces[coordinateToRetrieve.X - 1, coordinateToRetrieve.Y - 1];
        }

        public bool DoesPieceExistAt(BoardCoordinate coordinateToCheck)
        {
            return GetPiece(coordinateToCheck) != null;
        }

        public IEnumerable<BoardCoordinate> GetMovesFrom(BoardCoordinate originCoordinate)
        {
            Piece piece = GetPiece(originCoordinate);
            IEnumerable<BoardCoordinate> allPossibleMoves = piece.GetMovesFrom(originCoordinate);

            return allPossibleMoves.Where(move => IsMoveLegal(originCoordinate, move));
        }

        private bool IsMoveLegal(BoardCoordinate origin, BoardCoordinate destination)
        {
            bool isCapture = destination.IsCoordinateValidForBoardSize(boardSize) && GetPiece(destination) != null;

            bool isIlligalCapture = isCapture && !GetPiece(origin).IsCaptureAllowed(origin, destination);
            bool isIlligalNonCapture = !isCapture && !GetPiece(origin).IsNonCaptureAllowed(origin, destination);

            return !isIlligalCapture
                   && !isIlligalNonCapture
                   && destination.IsCoordinateValidForBoardSize(boardSize)
                   && !IsBlocked(origin, destination)
                   && !DoesFriendlyPieceExistAt(origin, destination);
        }

        private bool DoesFriendlyPieceExistAt(BoardCoordinate origin, BoardCoordinate destination)
        {
            var piece = GetPiece(destination);

            return piece != null && piece.IsFirstPlayerPiece == GetPiece(origin).IsFirstPlayerPiece;
        }

        private void SetPiece(Piece piece, BoardCoordinate location)
        {
            pieces[location.X - 1, location.Y - 1] = piece;
        }

        private void VerifyCoordinatesOrThrow(params BoardCoordinate[] coordinates)
        {
            if (coordinates.Any(bc => !bc.IsCoordinateValidForBoardSize(boardSize)))
            {
                throw new ArgumentException("coordinate");
            }
        }

        private bool IsBlocked(BoardCoordinate origin, BoardCoordinate destination)
        {
            var pathMaker = new PathMaker(origin, destination);

            List<BoardCoordinate> spacesAlongPath = pathMaker.GetPathToDestination().ToList();

            BoardCoordinate lastSpace = spacesAlongPath.LastOrDefault();

            return spacesAlongPath.Any(space => DoesFriendlyPieceExistAt(origin, space))
                   || spacesAlongPath.Any(space => DoesPieceExistAt(space) && !space.Equals(lastSpace));
        }
    }
}