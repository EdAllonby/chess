﻿using System.Collections.Generic;
using System.Linq;

namespace Chess
{
    public class Bishop : Piece
    {
        public Bishop(bool isFirstPlayerPiece = true) : base(isFirstPlayerPiece)
        {
        }

        public override IEnumerable<BoardCoordinate> GetMovesFrom(BoardCoordinate startingLocation, int boardSize = Board.DefaultBoardSize)
        {
            IEnumerable<int> allDistancesFromStart = Enumerable.Range(1, boardSize + 1);
            IEnumerable<BoardCoordinate> allPossibleCoordinates = allDistancesFromStart.SelectMany(sp => GetRadialDiagonalFrom(startingLocation, sp));
            IEnumerable<BoardCoordinate> legalBoardCoordinates = allPossibleCoordinates.Where(bc => bc.IsCoordinateValidForBoardSize(boardSize));
            return legalBoardCoordinates;
        }
    }
}