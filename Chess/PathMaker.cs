using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Chess
{
    public class PathMaker
    {
        private readonly BoardCoordinate destination;
        private readonly BoardCoordinate origin;

        public PathMaker(BoardCoordinate origin, BoardCoordinate destination)
        {
            this.origin = origin;
            this.destination = destination;
        }

        [Pure]
        public IEnumerable<BoardCoordinate> GetPathToDestination()
        {
            if (origin.IsOnSameHorizontalPathAs(destination))
            {
                return GetHorizontalPathSpaces();
            }

            if (origin.IsOnSameVerticalPathAs(destination))
            {
                return GetVerticalPathSpaces();
            }
            if (origin.IsOnSameDiagonalPathAs(destination))
            {
                return GetDiagonalPathSpaces();
            }

            return Enumerable.Empty<BoardCoordinate>();
        }

        private IEnumerable<BoardCoordinate> GetHorizontalPathSpaces()
        {
            IEnumerable<int> xCoordinatesToCheck = Enumerable.Empty<int>();

            if (origin.X < destination.X)
            {
                xCoordinatesToCheck = Enumerable.Range(origin.X + 1, destination.X - origin.X);
            }
            if (origin.X > destination.X)
            {
                xCoordinatesToCheck = Enumerable.Range(destination.X, origin.X - destination.X).Reverse();
            }

            return xCoordinatesToCheck.Select(x => BoardCoordinate.For(x, origin.Y));
        }

        private IEnumerable<BoardCoordinate> GetVerticalPathSpaces()
        {
            IEnumerable<int> yCoordinatesToCheck = Enumerable.Empty<int>();

            if (origin.Y < destination.Y)
            {
                yCoordinatesToCheck = Enumerable.Range(origin.Y + 1, destination.Y - origin.Y);
            }
            if (origin.Y > destination.Y)
            {
                yCoordinatesToCheck = Enumerable.Range(destination.Y, origin.Y - destination.Y).Reverse();
            }

            return yCoordinatesToCheck.Select(y => BoardCoordinate.For(origin.X, y));
        }

        private IEnumerable<BoardCoordinate> GetDiagonalPathSpaces()
        {
            int absoluteDistance = Math.Abs(origin.X - destination.X);
            int xDirection = (destination.X - origin.X)/absoluteDistance;
            int yDirection = (destination.Y - origin.Y)/absoluteDistance;

            return Enumerable.Range(1, absoluteDistance).Select(i => BoardCoordinate.For(origin.X + i*xDirection, origin.Y + i*yDirection));
        }
    }
}