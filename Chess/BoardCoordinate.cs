﻿using System;
using System.Diagnostics.Contracts;

namespace Chess
{
    public struct BoardCoordinate
    {
        private readonly int x;
        private readonly int y;

        public BoardCoordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public bool IsCoordinateValidForBoardSize(int boardSize)
        {
            return IsDimensionValidForBoardSize(X, boardSize) && IsDimensionValidForBoardSize(Y, boardSize);
        }

        private static bool IsDimensionValidForBoardSize(int dimensionValue, int boardSize)
        {
            return dimensionValue > 0 && dimensionValue <= boardSize;
        }

        public bool IsOnSameVerticalPathAs(BoardCoordinate other)
        {
            return X == other.X;
        }

        public bool IsOnSameHorizontalPathAs(BoardCoordinate other)
        {
            return Y == other.Y;
        }
        
        public bool IsOnSameDiagonalPathAs(BoardCoordinate other)
        {
            return Math.Abs(X - other.X) == Math.Abs(Y - other.Y);
        }

        public bool Matches(int expectedX, int expectedY)
        {
            return x == expectedX && y == expectedY;
        }

        public static BoardCoordinate For(int x, int y)
        {
            return new BoardCoordinate(x, y);
        }
    }
}