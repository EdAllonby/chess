﻿using System.Collections.Generic;
using System.Linq;

namespace Chess
{
    public class Rook : Piece
    {
        public Rook(bool isFirstPlayerPiece = true) : base(isFirstPlayerPiece)
        {
        }

        public override IEnumerable<BoardCoordinate> GetMovesFrom(BoardCoordinate startingLocation, int boardSize = Board.DefaultBoardSize)
        {
            List<int> availableCoordinates = Enumerable.Range(1, boardSize).ToList();

            IEnumerable<BoardCoordinate> verticalMoves = availableCoordinates.Where(y => startingLocation.Y != y)
                .Select(y => new BoardCoordinate(startingLocation.X, y));

            IEnumerable<BoardCoordinate> horizontalMoves = availableCoordinates.Where(x => startingLocation.X != x)
                .Select(x => new BoardCoordinate(x, startingLocation.Y));

            return verticalMoves.Union(horizontalMoves);
        }
    }
}