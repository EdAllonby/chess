﻿using System;

namespace Chess
{
    public class AsciiBoardBuilder
    {
        private readonly Board board = new Board();

        public Board GenerateBoard()
        {
            return board;
        }

        public void AddPiece(BoardCoordinate square, string piece)
        {
            VerifyNonEmptyPieceStringOrThrow(piece);

            char playerCode = piece[0];
            string pieceCode = piece.Substring(1);

            bool isFirstPlayerPiece = DetermineIsFirstPlayer(playerCode);

            Piece pieceToAdd = BuildPiece(pieceCode, isFirstPlayerPiece);

            board.AddPiece(pieceToAdd, square);
        }

        private static void VerifyNonEmptyPieceStringOrThrow(string piece)
        {
            if (piece == null)
            {
                throw new ArgumentNullException("piece");
            }
            if (string.IsNullOrEmpty(piece))
            {
                throw new ArgumentException("piece");
            }
        }

        private static bool DetermineIsFirstPlayer(char pieceColourCode)
        {
            if (pieceColourCode != 'W' && pieceColourCode != 'B')
            {
                throw new ArgumentException("Invalid Piece Colouring");
            }

            return pieceColourCode == 'W';
        }

        private static Piece BuildPiece(string pieceString, bool isFirstPlayerPiece)
        {
            switch (pieceString)
            {
                case "P":
                    return new Pawn(isFirstPlayerPiece);
                case "B":
                    return new Bishop(isFirstPlayerPiece);
                case "Q":
                    return new Queen(isFirstPlayerPiece);
                case "K":
                    return new King(isFirstPlayerPiece);
                case "R":
                    return new Rook(isFirstPlayerPiece);
                case "Kn":
                    return new Knight(isFirstPlayerPiece);
                default:
                    return null;
            }
        }
    }

}
