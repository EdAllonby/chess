﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Chess.AcceptanceTests
{
    [Binding]
    public class NormalBoardSetupScenarios
    {
        [When(@"there is a chess board set up as")]
        public void SetBoardInContextFromTable(Table table)
        {
            Board generatedBoard = BuildBoardFromTable(table);
            SetInContext(generatedBoard);
        }

        [When(@"there is a move from \((.*),(.*)\) to \((.*),(.*)\)")]
        public void ThereIsAMoveFrom(int startX, int startY, int destinationX, int destinationY)
        {
            var board = GetFromContext<Board>();
            board.MovePiece(BoardCoordinate.For(startX, startY), BoardCoordinate.For(destinationX, destinationY));
        }
        
        [Then(@"the piece at \((.*),(.*)\) should have exactly the following moves")]
        public void ThenThePieceAtShouldHaveExactlyTheFollowingMoves(int xCoordinate, int yCoordinate, Table table)
        {
            var board = GetFromContext<Board>();

            IEnumerable<BoardCoordinate> moves = board.GetMovesFrom(BoardCoordinate.For(xCoordinate, yCoordinate));
            IEnumerable<BoardCoordinate> coordinates = GetCoordinatesFromTable(table);

            CollectionAssert.AreEquivalent(coordinates, moves);
        }

        private static Board BuildBoardFromTable(Table table)
        {
            var builder = new AsciiBoardBuilder();

            List<int> indices = Enumerable.Range(0, 8).ToList();

            indices.ForEach(ri => indices.ForEach(ci => AddNonEmptyPiece(builder, table, ri, ci)));

            Board generateBoard = builder.GenerateBoard();
            return generateBoard;
        }

        private static void AddNonEmptyPiece(AsciiBoardBuilder builder, Table table, int rowIndex, int columnIndex)
        {
            int xCoordinate = columnIndex + 1;
            int yCoordinate = 8 - rowIndex;

            string pieceString = table.Rows[rowIndex][columnIndex];

            if (!string.IsNullOrEmpty(pieceString))
            {
                builder.AddPiece(BoardCoordinate.For(xCoordinate, yCoordinate), pieceString);
            }
        }

        private static IEnumerable<BoardCoordinate> GetCoordinatesFromTable(Table tableOfBoardCoordinates)
        {
            return tableOfBoardCoordinates.Rows.Select(row => BoardCoordinate.For(int.Parse(row[0]), int.Parse(row[1])));
        }

        private static T GetFromContext<T>()
        {
            return ScenarioContext.Current.Get<T>();
        }

        private static void SetInContext<T>(T data)
        {
            ScenarioContext.Current.Set(data);
        }
    }
}