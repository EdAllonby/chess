﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Chess.AcceptanceTests
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Queen Movement")]
    public partial class QueenMovementFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "QueenMoves.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Queen Movement", "", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("A queen in its beginning position")]
        public virtual void AQueenInItsBeginningPosition()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("A queen in its beginning position", ((string[])(null)));
#line 3
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table1 = new TechTalk.SpecFlow.Table(new string[] {
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8"});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table1.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "WQ",
                        "",
                        "",
                        "",
                        ""});
#line 4
 testRunner.When("there is a chess board set up as", ((string)(null)), table1, "When ");
#line hidden
            TechTalk.SpecFlow.Table table2 = new TechTalk.SpecFlow.Table(new string[] {
                        "X",
                        "Y"});
            table2.AddRow(new string[] {
                        "1",
                        "1"});
            table2.AddRow(new string[] {
                        "2",
                        "1"});
            table2.AddRow(new string[] {
                        "3",
                        "1"});
            table2.AddRow(new string[] {
                        "5",
                        "1"});
            table2.AddRow(new string[] {
                        "6",
                        "1"});
            table2.AddRow(new string[] {
                        "7",
                        "1"});
            table2.AddRow(new string[] {
                        "8",
                        "1"});
            table2.AddRow(new string[] {
                        "4",
                        "2"});
            table2.AddRow(new string[] {
                        "4",
                        "3"});
            table2.AddRow(new string[] {
                        "4",
                        "4"});
            table2.AddRow(new string[] {
                        "4",
                        "5"});
            table2.AddRow(new string[] {
                        "4",
                        "6"});
            table2.AddRow(new string[] {
                        "4",
                        "7"});
            table2.AddRow(new string[] {
                        "4",
                        "8"});
            table2.AddRow(new string[] {
                        "3",
                        "2"});
            table2.AddRow(new string[] {
                        "2",
                        "3"});
            table2.AddRow(new string[] {
                        "1",
                        "4"});
            table2.AddRow(new string[] {
                        "5",
                        "2"});
            table2.AddRow(new string[] {
                        "6",
                        "3"});
            table2.AddRow(new string[] {
                        "7",
                        "4"});
            table2.AddRow(new string[] {
                        "8",
                        "5"});
#line 15
 testRunner.Then("the piece at (4,1) should have exactly the following moves", ((string)(null)), table2, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Queens in their beginning positions when all pieces are present")]
        public virtual void QueensInTheirBeginningPositionsWhenAllPiecesArePresent()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Queens in their beginning positions when all pieces are present", ((string[])(null)));
#line 39
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table3 = new TechTalk.SpecFlow.Table(new string[] {
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8"});
            table3.AddRow(new string[] {
                        "BR",
                        "BKn",
                        "BB",
                        "BQ",
                        "BK",
                        "BB",
                        "BKn",
                        "BR"});
            table3.AddRow(new string[] {
                        "BP",
                        "BP",
                        "BP",
                        "BP",
                        "BP",
                        "BP",
                        "BP",
                        "BP"});
            table3.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table3.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table3.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table3.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table3.AddRow(new string[] {
                        "WP",
                        "WP",
                        "WP",
                        "WP",
                        "WP",
                        "WP",
                        "WP",
                        "WP"});
            table3.AddRow(new string[] {
                        "WR",
                        "WKn",
                        "WB",
                        "WQ",
                        "WK",
                        "WB",
                        "WKn",
                        "WR"});
#line 40
 testRunner.When("there is a chess board set up as", ((string)(null)), table3, "When ");
#line hidden
            TechTalk.SpecFlow.Table table4 = new TechTalk.SpecFlow.Table(new string[] {
                        "X",
                        "Y"});
#line 51
 testRunner.Then("the piece at (4,1) should have exactly the following moves", ((string)(null)), table4, "Then ");
#line hidden
            TechTalk.SpecFlow.Table table5 = new TechTalk.SpecFlow.Table(new string[] {
                        "X",
                        "Y"});
#line 53
 testRunner.And("the piece at (5,8) should have exactly the following moves", ((string)(null)), table5, "And ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("A queen with a capture opportunity")]
        public virtual void AQueenWithACaptureOpportunity()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("A queen with a capture opportunity", ((string[])(null)));
#line 56
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table6 = new TechTalk.SpecFlow.Table(new string[] {
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8"});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "BQ",
                        "",
                        "",
                        "",
                        ""});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table6.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "WQ",
                        "",
                        "",
                        "",
                        ""});
#line 57
 testRunner.When("there is a chess board set up as", ((string)(null)), table6, "When ");
#line hidden
            TechTalk.SpecFlow.Table table7 = new TechTalk.SpecFlow.Table(new string[] {
                        "X",
                        "Y"});
            table7.AddRow(new string[] {
                        "1",
                        "1"});
            table7.AddRow(new string[] {
                        "2",
                        "1"});
            table7.AddRow(new string[] {
                        "3",
                        "1"});
            table7.AddRow(new string[] {
                        "5",
                        "1"});
            table7.AddRow(new string[] {
                        "6",
                        "1"});
            table7.AddRow(new string[] {
                        "7",
                        "1"});
            table7.AddRow(new string[] {
                        "8",
                        "1"});
            table7.AddRow(new string[] {
                        "4",
                        "2"});
            table7.AddRow(new string[] {
                        "4",
                        "3"});
            table7.AddRow(new string[] {
                        "4",
                        "4"});
            table7.AddRow(new string[] {
                        "4",
                        "5"});
            table7.AddRow(new string[] {
                        "4",
                        "6"});
            table7.AddRow(new string[] {
                        "4",
                        "7"});
            table7.AddRow(new string[] {
                        "4",
                        "8"});
            table7.AddRow(new string[] {
                        "3",
                        "2"});
            table7.AddRow(new string[] {
                        "2",
                        "3"});
            table7.AddRow(new string[] {
                        "1",
                        "4"});
            table7.AddRow(new string[] {
                        "5",
                        "2"});
            table7.AddRow(new string[] {
                        "6",
                        "3"});
            table7.AddRow(new string[] {
                        "7",
                        "4"});
            table7.AddRow(new string[] {
                        "8",
                        "5"});
#line 68
 testRunner.Then("the piece at (4,1) should have exactly the following moves", ((string)(null)), table7, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("A white queen surrounded by enemy pieces")]
        public virtual void AWhiteQueenSurroundedByEnemyPieces()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("A white queen surrounded by enemy pieces", ((string[])(null)));
#line 92
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table8 = new TechTalk.SpecFlow.Table(new string[] {
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8"});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "BQ",
                        "",
                        "",
                        "",
                        ""});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "BB",
                        "BB",
                        "BP",
                        "",
                        "",
                        ""});
            table8.AddRow(new string[] {
                        "",
                        "",
                        "BR",
                        "WQ",
                        "BR",
                        "",
                        "",
                        ""});
#line 93
 testRunner.When("there is a chess board set up as", ((string)(null)), table8, "When ");
#line hidden
            TechTalk.SpecFlow.Table table9 = new TechTalk.SpecFlow.Table(new string[] {
                        "X",
                        "Y"});
            table9.AddRow(new string[] {
                        "3",
                        "1"});
            table9.AddRow(new string[] {
                        "5",
                        "1"});
            table9.AddRow(new string[] {
                        "3",
                        "2"});
            table9.AddRow(new string[] {
                        "4",
                        "2"});
            table9.AddRow(new string[] {
                        "5",
                        "2"});
#line 104
 testRunner.Then("the piece at (4,1) should have exactly the following moves", ((string)(null)), table9, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("A black queen surrounded by enemy pieces")]
        public virtual void ABlackQueenSurroundedByEnemyPieces()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("A black queen surrounded by enemy pieces", ((string[])(null)));
#line 112
this.ScenarioSetup(scenarioInfo);
#line hidden
            TechTalk.SpecFlow.Table table10 = new TechTalk.SpecFlow.Table(new string[] {
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7",
                        "8"});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "WR",
                        "BQ",
                        "WR",
                        "",
                        "",
                        ""});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "WB",
                        "WB",
                        "WP",
                        "",
                        "",
                        ""});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""});
            table10.AddRow(new string[] {
                        "",
                        "",
                        "",
                        "WQ",
                        "",
                        "",
                        "",
                        ""});
#line 113
 testRunner.When("there is a chess board set up as", ((string)(null)), table10, "When ");
#line hidden
            TechTalk.SpecFlow.Table table11 = new TechTalk.SpecFlow.Table(new string[] {
                        "X",
                        "Y"});
            table11.AddRow(new string[] {
                        "3",
                        "8"});
            table11.AddRow(new string[] {
                        "5",
                        "8"});
            table11.AddRow(new string[] {
                        "3",
                        "7"});
            table11.AddRow(new string[] {
                        "4",
                        "7"});
            table11.AddRow(new string[] {
                        "5",
                        "7"});
#line 124
 testRunner.Then("the piece at (4,8) should have exactly the following moves", ((string)(null)), table11, "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
