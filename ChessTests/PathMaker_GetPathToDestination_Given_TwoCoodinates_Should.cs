﻿using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests
{
    [TestFixture]
    public class PathMaker_GetPathToDestination_Given_TwoCoodinates_Should
    {
        [Test]
        public void Return_7_Spaces()
        {
            var target = new PathMaker(BoardCoordinate.For(1, 1), BoardCoordinate.For(8, 1));

            Assert.AreEqual(7, target.GetPathToDestination().Count());
        }

        [Test]
        public void Return_Empty_Collection_For_11_And_23()
        {
            var origin = BoardCoordinate.For(1, 1);
            var destination = BoardCoordinate.For(2, 3);

            var target = new PathMaker(origin, destination);

            Assert.IsEmpty(target.GetPathToDestination());
        }

        [Test]
        public void ReturnEmptyCollectionForOriginSameAsDestination()
        {
            var origin = BoardCoordinate.For(1, 1);
            var destination = BoardCoordinate.For(1, 1);

            var target = new PathMaker(origin, destination);

            Assert.IsFalse(target.GetPathToDestination().Any());
        }

        [Test]
        public void ReturnSinglePointForSingleMove()
        {
            var origin = BoardCoordinate.For(4, 8);
            var destination = BoardCoordinate.For(4, 7);

            var target  = new PathMaker(origin, destination);

            Assert.IsTrue(target.GetPathToDestination().First().Matches(4, 7));
        }
    }
}