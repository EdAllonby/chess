﻿using System.Collections.Generic;
using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests
{
    [TestFixture]
    public class QueenTest
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Queen();
            movesFrom11 = Target.GetMovesFrom(new BoardCoordinate(1, 1));
        }

        private Queen Target { get; set; }
        private IEnumerable<BoardCoordinate> movesFrom11;

        [TestFixture]
        public class GetMovesFrom : QueenTest
        {
            [Test]
            public void DoesNotReturn00From11()
            {
                Assert.IsFalse(movesFrom11.Any(bc => bc.X == 0 || bc.Y == 0));
            }

            [Test]
            public void Returns12From11()
            {
                Assert.IsTrue(movesFrom11.Any(boardCoordinate => boardCoordinate.X == 1 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns22From11()
            {
                Assert.IsTrue(movesFrom11.Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns33From11()
            {
                Assert.IsTrue(movesFrom11.Any(boardCoordinate => boardCoordinate.X == 3 && boardCoordinate.Y == 3));
            }
        }
    }
}