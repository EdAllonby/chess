﻿using System;
using Chess;
using NUnit.Framework;

namespace ChessTests.AsciiBoardBuilderTests
{
    [TestFixture]
    public class AsciiBoardBuilder_GenerateBoard_Should
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new AsciiBoardBuilder();
        }

        private AsciiBoardBuilder Target { get; set; }

        private Board GeneratedBoard
        {
            get { return Target.GenerateBoard(); }
        }

        private BoardCoordinate SomeCoordinate
        {
            get { return BoardCoordinate.For(1, 1); }
        }

        [Test]
        public void GenerateBoard_WithA_WhiteBishop_WhenWBIsAdded()
        {
            Target.AddPiece(SomeCoordinate, "WB");
            Piece addedPiece = GeneratedBoard.GetPiece(SomeCoordinate);

            bool isWhiteBishop = addedPiece.GetType() == typeof (Bishop) && addedPiece.IsFirstPlayerPiece;

            Assert.IsTrue(isWhiteBishop);
        }

        [Test]
        public void GenerateBoard_WithKing_WhenBQIsAdded()
        {
            Target.AddPiece(SomeCoordinate, "BK");
            var addedPiece = GeneratedBoard.GetPiece(SomeCoordinate);

            Assert.IsInstanceOf<King>(addedPiece);
        }

        [Test]
        public void GenerateBoard_WithOnePiece_WhenSinglePawn_IsAdded()
        {
            Target.AddPiece(SomeCoordinate, "WP");

            Assert.AreEqual(1, GeneratedBoard.PieceCount);
        }

        [Test]
        public void GenerateBoard_WithQueen_WhenWQIsAdded()
        {
            Target.AddPiece(SomeCoordinate, "WQ");

            var addedPiece = GeneratedBoard.GetPiece(SomeCoordinate);

            Assert.IsInstanceOf<Queen>(addedPiece);
        }

        [Test]
        public void GenerateBoard_WithTwoPieces_WhenTwoPawns_AreAdded()
        {
            Target.AddPiece(SomeCoordinate, "WP");
            Target.AddPiece(BoardCoordinate.For(1, 2), "WP");

            Assert.AreEqual(2, GeneratedBoard.PieceCount);
        }

        [Test]
        public void GenerateBoardWithA_BlackBishop_WhenBBIsAdded()
        {
            Target.AddPiece(SomeCoordinate, "BB");
            var addedPiece = GeneratedBoard.GetPiece(SomeCoordinate);

            var isBlackBishop = addedPiece.GetType() == typeof (Bishop) && !addedPiece.IsFirstPlayerPiece;

            Assert.IsTrue(isBlackBishop);
        }

        [Test]
        public void GenerateABoardWith_Rook_When_WR_IsAdded()
        {
            Target.AddPiece(SomeCoordinate, "WR");
            Piece addedPiece = GeneratedBoard.GetPiece(SomeCoordinate);

            Assert.IsInstanceOf<Rook>(addedPiece);
        }

        [Test]
        public void GenerateBoardWith_Knight_When_BKn_IsAdded()
        {
            Target.AddPiece(SomeCoordinate, "BKn");
            var addedPiece = GeneratedBoard.GetPiece(SomeCoordinate);

            Assert.IsInstanceOf<Knight>(addedPiece);
        }

        [Test]
        public void GenerateEmptyBoard()
        {
            Assert.AreEqual(0, GeneratedBoard.PieceCount);
        }

        [Test]
        public void ThrowExceptionWhenColourIsNotSpecified()
        {
            Assert.Throws<ArgumentException>(() => Target.AddPiece(SomeCoordinate, "P"));
        }

        [Test]
        public void ThrowArgumentException_WhenFirstCharacter_IsNotBOrW()
        {
            Assert.Throws<ArgumentException>(() => Target.AddPiece(SomeCoordinate, "DD"));
        }

        [Test]
        public void Throw_ArgumentNullException_On_Null_PieceString()
        {
            Assert.Throws<ArgumentNullException>(() => Target.AddPiece(SomeCoordinate, null));
        }

        [Test]
        public void Throw_ArgumentException_On_Empty_PieceString()
        {
            Assert.Throws<ArgumentException>(() => Target.AddPiece(SomeCoordinate, String.Empty));
        }

        [Test]
        public void Throw_ArgumentException_For_Single_Character_Piece()
        {
            Assert.Throws<ArgumentException>(() => Target.AddPiece(SomeCoordinate, "1"));
        }
    }
}