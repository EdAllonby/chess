﻿using System.Collections.Generic;
using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests
{
    [TestFixture]
    public class PawnTest
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Pawn();
            movesFrom22 = Target.GetMovesFrom(new BoardCoordinate(2, 2));
        }

        private Pawn Target { get; set; }
        private IEnumerable<BoardCoordinate> movesFrom22;

        [TestFixture]
        public class GetMovesFrom : PawnTest
        {
            [Test]
            public void DoesNotReturn24WhenPassed22IfPieceHasAlreadyMoved()
            {
                Target.HasMoved = true;
                var possibleMoves = movesFrom22;

                Assert.IsFalse(possibleMoves.Any(bc => bc.X == 2 && bc.Y == 4));
            }

            [Test]
            public void Returns23AsFirstResultWhenPassed22()
            {
                IEnumerable<BoardCoordinate> possibleMoves = movesFrom22;

                Assert.IsTrue(possibleMoves.Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 3));
            }

            [Test]
            public void Returns24AsOneResultWhenPassed22()
            {
                var possibleMoves = movesFrom22;

                Assert.IsTrue(possibleMoves.Any(bc => bc.X == 2 && bc.Y == 4));
            }
        }

        [TestFixture]
        public class IsCaptureAllowed : PawnTest
        {
            private bool CanCaptureFrom22(int xCoordinate, int yCoordinate)
            {
                return Target.IsCaptureAllowed(BoardCoordinate.For(2, 2), BoardCoordinate.For(xCoordinate, yCoordinate));
            }

            [Test]
            public void Returns_False_For_Some_Oblique_Square()
            {
                Assert.IsFalse(CanCaptureFrom22(6, 3));
            }

            [Test]
            public void Returns_False_For_Square_In_Front_Of_Pawn()
            {
                Assert.IsFalse(CanCaptureFrom22(2, 3));
            }

            [Test]
            public void Returns_True_For_RightOne_UpOne()
            {
                Assert.IsTrue(CanCaptureFrom22(3, 3));
            }

            [Test]
            public void Returns_True_For_Square_Diagonally_In_Front_Of_Pawn()
            {
                Assert.IsTrue(CanCaptureFrom22(1, 3));
            }

            [Test]
            public void Returns_False_For_RightOne_UpTwo()
            {
                Assert.IsFalse(CanCaptureFrom22(3, 4));
            }
        }
    }
}