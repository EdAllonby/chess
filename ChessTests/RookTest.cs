﻿using System.Collections.Generic;
using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests
{
    [TestFixture]
    public class RookTest
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Rook();
            movesFrom11 = Target.GetMovesFrom(new BoardCoordinate(1, 1));
        }

        private Rook Target { get; set; }
        private IEnumerable<BoardCoordinate> movesFrom11;

        [TestFixture]
        public class GetMovesFrom : RookTest
        {
            [Test]
            public void Returns74From71()
            {
                var moves = Target.GetMovesFrom(new BoardCoordinate(7, 1));

                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 7 && boardCoordinate.Y == 4));
            }

            [Test]
            public void Returns7HorizontalMovesWithBoardSize8()
            {
                Assert.AreEqual(7, movesFrom11.Count(bc => bc.Y == 1));
            }

            [Test]
            public void Returns7VerticalMovesWithBoardSize8()
            {
                Assert.AreEqual(7, movesFrom11.Count(bc => bc.X == 1));
            }

            [Test]
            public void ReturnsNoMovesThatContainsAZero()
            {
                Assert.AreEqual(0, movesFrom11.Count(boardCoordinate => boardCoordinate.X == 0 || boardCoordinate.Y == 0));
            }
        }
    }
}