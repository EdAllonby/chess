﻿using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests
{
    [TestFixture]
    public class BishopTest
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Bishop();
        }

        private Bishop Target { get; set; }

        [TestFixture]
        public class GetMovesFrom : BishopTest
        {
            [Test]
            public void DoesNotReturnZeroOrNegativeValuesForBoardCoordinates()
            {
                Assert.IsFalse(Target.GetMovesFrom(new BoardCoordinate(1, 2)).Any(boardCoordinate => boardCoordinate.X <= 0 || boardCoordinate.Y <= 0));
            }

            [Test]
            public void Returns11Form22()
            {
                Assert.IsTrue(Target.GetMovesFrom(new BoardCoordinate(2, 2)).Any(boardCoordinate => boardCoordinate.X == 1 && boardCoordinate.Y == 1));
            }

            [Test]
            public void Returns21From12()
            {
                Assert.IsTrue(Target.GetMovesFrom(new BoardCoordinate(1, 2)).Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 1));
            }

            [Test]
            public void Returns22From11()
            {
                Assert.IsTrue(Target.GetMovesFrom(new BoardCoordinate(1, 1)).Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns81For18()
            {
                Assert.IsTrue(Target.GetMovesFrom(new BoardCoordinate(1, 8)).Any(boardCoordinate => boardCoordinate.X == 8 && boardCoordinate.Y == 1));
            }

            [Test]
            public void Returns88For11()
            {
                Assert.IsTrue(Target.GetMovesFrom(new BoardCoordinate(1, 1)).Any(boardCoordinate => boardCoordinate.X == 8 && boardCoordinate.Y == 8));
            }

            [Test]
            public void ReturnsNonEmptyCollection()
            {
                Assert.IsTrue(Target.GetMovesFrom(new BoardCoordinate(1, 1)).Any());
            }
        }
    }
}