﻿using System;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class Board_Constructor_Should
    {
        [Test]
        public void Throw_ArgumentException_On_Negative_BoardSize()
        {
            Assert.Throws<ArgumentException>(() => new Board(-1));
        }

        [Test]
        public void Throw_ArgumentException_On_Zero_BoardSize()
        {
            Assert.Throws<ArgumentException>(() => new Board(0));
        }
    }
}