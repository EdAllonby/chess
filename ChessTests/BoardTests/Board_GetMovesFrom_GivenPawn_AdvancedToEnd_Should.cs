﻿using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class Board_GetMovesFrom_GivenPawn_AdvancedToEnd_Should
    {
        private Board Target { get; set; }

        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();

            var startingPosition = new BoardCoordinate(1, 7);

            Target.AddPiece(new Pawn(), startingPosition);
            Target.MovePiece(startingPosition, new BoardCoordinate(1, 8));
        }

        [Test]
        public void NotBomb()
        {
            Assert.AreEqual(0, Target.GetMovesFrom(BoardCoordinate.For(1, 8)).Count());
        }
    }
}