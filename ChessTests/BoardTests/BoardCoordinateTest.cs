﻿using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    internal class BoardCoordinateTest
    {
        [TestFixture]
        public class IsCoordinateValidForBoardSize
        {
            [Test]
            public void ReturnsFalseForXLessThanZero()
            {
                var coordinate = new BoardCoordinate(-12, 2);

                Assert.IsFalse(coordinate.IsCoordinateValidForBoardSize(5));
            }

            [Test]
            public void ReturnsFalseForYLessThanZero()
            {
                var coordinate = new BoardCoordinate(2, -12);

                Assert.IsFalse(coordinate.IsCoordinateValidForBoardSize(5));
            }
        }
    }
}