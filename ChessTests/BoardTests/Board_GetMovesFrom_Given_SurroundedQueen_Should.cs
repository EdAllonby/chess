﻿using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class Board_GetMovesFrom_Given_SurroundedQueen_Should
    {
        private Board Target { get; set; }

        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();
            Target.AddPiece(new Rook(false), BoardCoordinate.For(3, 1));
            Target.AddPiece(new Rook(false), BoardCoordinate.For(5, 1));
            Target.AddPiece(new Bishop(false), BoardCoordinate.For(3, 2));
            Target.AddPiece(new Bishop(false), BoardCoordinate.For(4, 2));
            Target.AddPiece(new Pawn(false), BoardCoordinate.For(5, 2));

            Target.AddPiece(new Queen(), BoardCoordinate.For(4, 1));
        }

        [Test]
        public void Not_Return_MoveAt11()
        {
            IEnumerable<BoardCoordinate> moves = Target.GetMovesFrom(BoardCoordinate.For(4, 1));
            Assert.IsFalse(moves.Any(bc => bc.Matches(1, 1)));
        }
    }
}