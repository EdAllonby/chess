﻿using System;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class BoardRemovePieceGivenChessBoardWithoutPawnsShould
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();

            var piecePositioner = new PiecePositioner(Target);
            piecePositioner.SetupStandardPieces(1);
        }

        private Board Target { get; set; }

        [Test]
        public void ResultInGetPieceFor11BeingNullWhenCalledWith11()
        {
            Target.RemovePiece(BoardCoordinate.For(1, 1));

            Assert.IsFalse(Target.DoesPieceExistAt(BoardCoordinate.For(1, 1)));
        }

        [Test]
        public void ThrowExceptionWhenCalledWith12()
        {
            Assert.Throws<ArgumentException>(() => Target.RemovePiece(BoardCoordinate.For(1, 2)));
        }
    }
}