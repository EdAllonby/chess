﻿using System;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public sealed class Board_AddPiece_Given_EmptyBoard_Should
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Piece = new Pawn();
            Target = new Board();
        }

        private Pawn Piece { get; set; }
        private Board Target { get; set; }

        [Test]
        public void AcceptRookAsArgumentForPiece()
        {
            var coordinate = new BoardCoordinate(1, 2);

            Target.AddPiece(new Rook(), coordinate);
        }

        [Test]
        public void NotThrowExceptionWhenAddingAPieceToAnUnoccupiedSquare()
        {
            Target.AddPiece(new Pawn(), new BoardCoordinate(2, 1));
        }

        [Test]
        public void ResultInValidPieceAt_99_When_BoardSize_Is9()
        {
            Target = new Board(9);

            Target.AddPiece(new Rook(), BoardCoordinate.For(9, 9));
        }

        [Test]
        public void ThrowExceptionOnNullArguments()
        {
            Assert.Throws<ArgumentNullException>(() => Target.AddPiece(null, BoardCoordinate.For(2, 3)));
        }

        [Test]
        public void ThrowExceptionWhenBoardCoordinateHasLargerXValueThanBoardSize()
        {
            var coordinate = new BoardCoordinate(9, 1);
            Assert.Throws<ArgumentException>(() => Target.AddPiece(new Pawn(), coordinate));
        }

        [Test]
        public void ThrowExceptionWhenBoardCoordinateHasLargerYValueThanBoardSize()
        {
            var coordinate = new BoardCoordinate(1, 9);
            Assert.Throws<ArgumentException>(() => Target.AddPiece(new Pawn(), coordinate));
        }

        [Test]
        public void ThrowExceptionWhenBoardCoordinateHasZeroXValue()
        {
            var coordinate = new BoardCoordinate(0, 9);
            Assert.Throws<ArgumentException>(() => Target.AddPiece(new Pawn(), coordinate));
        }
    }
}