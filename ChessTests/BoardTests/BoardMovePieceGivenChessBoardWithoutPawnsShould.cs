﻿using System;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class BoardMovePieceGivenChessBoardWithoutPawnsShould
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();
            OriginCoordinate = BoardCoordinate.For(1, 1);
            DestinationCoordinate = BoardCoordinate.For(1, 2);

            var piecePositioner = new PiecePositioner(Target);
            piecePositioner.SetupStandardPieces(1);
        }

        private BoardCoordinate OriginCoordinate { get; set; }
        private BoardCoordinate DestinationCoordinate { get; set; }
        private Board Target { get; set; }

        [Test]
        public void ResultInNotPiecedAt11WhenRookAt11IsMovedTo22()
        {
            Target.MovePiece(OriginCoordinate, DestinationCoordinate);

            Assert.IsFalse(Target.DoesPieceExistAt(OriginCoordinate));
        }

        [Test]
        public void ResultinPieceAt12WhenRookAt11IsMovedTo12()
        {
            Target.MovePiece(OriginCoordinate, DestinationCoordinate);

            Assert.IsTrue(Target.DoesPieceExistAt(DestinationCoordinate));
        }

        [Test]
        public void ResultInPieceAtOriginRelocatedToDestination()
        {
            Piece pieceThatWillBeMoved = Target.GetPiece(OriginCoordinate);

            Target.MovePiece(OriginCoordinate, DestinationCoordinate);

            Assert.AreEqual(pieceThatWillBeMoved, Target.GetPiece(DestinationCoordinate));
        }

        [Test]
        public void ThrowExceptionForOutOfBoundsArgument()
        {
            Assert.Throws<ArgumentException>(() => Target.MovePiece(BoardCoordinate.For(1, 9), BoardCoordinate.For(2, 2)));
        }

        [Test]
        public void ThrowExceptionForOutOfBoundsDestinationArgument()
        {
            Assert.Throws<ArgumentException>(() => Target.MovePiece(OriginCoordinate, BoardCoordinate.For(10, 10)));
        }

        [Test]
        public void ThrowExceptionWhenAttemptIsMadeToMoveNonexistentPiece()
        {
            Assert.Throws<ArgumentNullException>(() => Target.MovePiece(DestinationCoordinate, OriginCoordinate));
        }


        [Test]
        public void SetHasMoved_WhenAPiece_IsMoved()
        {
            Target.MovePiece(OriginCoordinate, DestinationCoordinate);

            Piece movedPiece = Target.GetPiece(DestinationCoordinate);

            Assert.IsTrue(movedPiece.HasMoved);
        }
    }
}