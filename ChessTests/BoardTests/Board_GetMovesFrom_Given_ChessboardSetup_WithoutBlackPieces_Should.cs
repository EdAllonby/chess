﻿using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class Board_GetMovesFrom_Given_ChessboardSetup_WithOnlyBlackKing_Should
    {
        private Board Target { get; set; }

        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();
            var positioner = new PiecePositioner(Target);
            positioner.SetupStandardPieces(1);
            positioner.SetupStandardPawns(2);

            Target.AddPiece(new King(false), BoardCoordinate.For(4, 8));
        }

        [Test]
        public void ReturnFiveMovesForAddedBlackKing()
        {
            int movesForBlackKing = Target.GetMovesFrom(BoardCoordinate.For(4,8)).Count();
            Assert.AreEqual(5, movesForBlackKing);
        }
    }
}
