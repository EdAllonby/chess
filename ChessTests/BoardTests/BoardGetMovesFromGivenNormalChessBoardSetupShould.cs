﻿using System.Collections.Generic;
using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class BoardGetMovesFromGivenNormalChessBoardSetupShould
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();


            var piecePositioner = new PiecePositioner(Target);

            piecePositioner.SetupStandardBoard();
        }

        private Board Target { get; set; }

        private IEnumerable<BoardCoordinate> MovesForLeftWhiteKnight
        {
            get { return Target.GetMovesFrom(BoardCoordinate.For(2, 1)); }
        }

        [Test]
        public void ReturnASetOfMovesForAPawnContainingSpaceTwoAhead()
        {
            var moves = Target.GetMovesFrom(new BoardCoordinate(1, 2));

            Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.Matches(1, 4)));
        }

        [Test]
        public void ReturnASetOfMovesForAPawnContainingTheSpaceOneAhead()
        {
            var moves = Target.GetMovesFrom(new BoardCoordinate(1, 2));

            Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.Matches(1, 3)));
        }

        [Test]
        public void ReturnEmptySetForRookAt11()
        {
            var moves = Target.GetMovesFrom(new BoardCoordinate(1, 1));

            Assert.IsFalse(moves.Any());
        }

        [Test]
        public void ReturnEmptySetForRookAt81()
        {
            var moves = Target.GetMovesFrom(new BoardCoordinate(8, 1));

            Assert.IsFalse(moves.Any());
        }

        [Test]
        public void ReturnEmptySetForRookAt88()
        {
            var moves = Target.GetMovesFrom(new BoardCoordinate(8, 8));

            Assert.IsFalse(moves.Any());
        }

        [Test]
        public void ReturnSet_With13_ForKnight_At21()
        {
            Assert.IsTrue(MovesForLeftWhiteKnight.Any(bc => bc.Matches(1, 3)));
        }

        [Test]
        public void ReturnSet_With33_ForKnight_At21()
        {
            Assert.IsTrue(MovesForLeftWhiteKnight.Any(bc => bc.Matches(3, 3)));
        }

        [Test]
        public void NotReturn_42_ForKnight_At21()
        {
            Assert.IsFalse(MovesForLeftWhiteKnight.Any(bc => bc.Matches(4, 2)));
        }
    }
}