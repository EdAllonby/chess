﻿using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    public class BoardGetMovesFromGivenChessboardSetupWithoutPawnsShould
    {
        private Board Target { get; set; }

        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();

            var piecePositioner = new PiecePositioner(Target);
            piecePositioner.SetupStandardPieces(1);
            piecePositioner.SetupStandardPieces(8, false);
        }

        [Test]
        public void ShouldReturnRookMovesContaining12()
        {
            var moves = Target.GetMovesFrom(BoardCoordinate.For(1, 1));

            Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.Matches(1, 2)));
        }

        [Test]
        public void ShouldReturnRookMovesNotContaining21()
        {
            var moves = Target.GetMovesFrom(BoardCoordinate.For(1, 1));

            Assert.IsFalse(moves.Any(boardCoordinate => boardCoordinate.Matches(2, 1)));
        }

        [Test]
        public void ShouldReturnKingMovesContaining52()
        {
            var moves = Target.GetMovesFrom(BoardCoordinate.For(5, 1));
            Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.Matches(5, 2)));
        }

        [Test]
        public void ReturnsEmptyForBishopAfterBlockingPawnsAreAdded()
        {
            Target.AddPiece(new Pawn(), BoardCoordinate.For(2, 2));
            Target.AddPiece(new Pawn(), BoardCoordinate.For(4, 2));

            var moves = Target.GetMovesFrom(BoardCoordinate.For(3, 1));

            Assert.IsFalse(moves.Any());
        }

        [Test]
        public void ReturnMovesForBishopThatIsNotBlocked()
        {
            Target.AddPiece(new Pawn(), BoardCoordinate.For(2, 2));
            var moves = Target.GetMovesFrom(BoardCoordinate.For(3, 1));

            Assert.IsTrue(moves.Any());
        }

        [Test]
        public void ReturnNonEmptyForBishopAt38()
        {
            var moves = Target.GetMovesFrom(BoardCoordinate.For(3, 8));

            Assert.IsTrue(moves.Any());
        }

        [Test]
        public void ReturnEmptyForBishopAt38WhenBlocked()
        {
            Target.AddPiece(new Pawn(false), BoardCoordinate.For(2, 7));
            Target.AddPiece(new Pawn(false), BoardCoordinate.For(4, 7));

            var moves = Target.GetMovesFrom(BoardCoordinate.For(3, 8));

            Assert.IsFalse(moves.Any());
        }

        [Test]
        public void Allow_Rook_At11_ToMoveTo_CaptureRook_At18()
        {
            var moves = Target.GetMovesFrom(BoardCoordinate.For(1, 1));

            Assert.IsTrue(moves.Any(m => m.Matches(1, 8)));
        }

        [Test]
        public void Not_Allow_Rook_At11_ToCaptureRook_At11_WhenAPawnIsInFrontOfIt()
        {
            Target.AddPiece(new Pawn(false), BoardCoordinate.For(1, 7));

            var moves = Target.GetMovesFrom(BoardCoordinate.For(1, 1));

            Assert.IsFalse(moves.Any(m => m.Matches(1, 8)));
        }
    }
}