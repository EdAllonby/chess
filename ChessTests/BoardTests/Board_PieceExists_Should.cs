﻿using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class Board_DoesPieceExistAt_Should
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Board();
            Coordinate = BoardCoordinate.For(1, 1);
        }

        private Board Target { get; set; }

        private static BoardCoordinate Coordinate { get; set; }
        
        [Test]
        public void ReturnFalse_ForEmptySquare()
        {
            Assert.IsFalse(Target.DoesPieceExistAt(Coordinate));
        }

        [Test]
        public void ReturnTrue_ForPopulatedSquare()
        {
            Target.AddPiece(new Rook(), Coordinate);

            Assert.IsTrue(Target.DoesPieceExistAt(Coordinate));
        }
    }
}