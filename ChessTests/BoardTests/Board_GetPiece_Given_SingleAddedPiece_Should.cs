﻿using Chess;
using NUnit.Framework;

namespace ChessTests.BoardTests
{
    [TestFixture]
    public class Board_GetPiece_Given_SingleAddedPiece_Should
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Piece = new Pawn();
            Target = new Board();

            Target.AddPiece(Piece, new BoardCoordinate(1, 1));
        }

        private Pawn Piece { get; set; }
        private Board Target { get; set; }

        [Test]
        public void RetrievesPieceAddedToLocation()
        {
            Assert.AreEqual(Piece, Target.GetPiece(new BoardCoordinate(1, 1)));
        }
    }
}