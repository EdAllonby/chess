﻿using System.Collections.Generic;
using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests
{
    [TestFixture]
    public class KnightTest
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new Knight();
        }

        private Knight Target { get; set; }

        private IEnumerable<BoardCoordinate> GetMoves(int x, int y)
        {
            return Target.GetMovesFrom(new BoardCoordinate(x, y));
        }

        [TestFixture]
        public class GetMovesFrom : KnightTest
        {
            [Test]
            public void DoesNotReturnIllegalMoves()
            {
                var moves = GetMoves(1, 1);
                Assert.IsFalse(moves.Any(boardCoordinate => boardCoordinate.X == -1 && boardCoordinate.Y == 0));
            }

            [Test]
            public void Returns12From33()
            {
                var moves = GetMoves(3, 3);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 1 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns14From33()
            {
                var moves = GetMoves(3, 3);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 1 && boardCoordinate.Y == 4));
            }

            [Test]
            public void Returns21From33()
            {
                var moves = GetMoves(3, 3);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 1));
            }

            [Test]
            public void Returns23From11()
            {
                var moves = GetMoves(1, 1);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 3));
            }

            [Test]
            public void Returns25From33()
            {
                var moves = GetMoves(3, 3);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 5));
            }

            [Test]
            public void Returns32From11()
            {
                var moves = GetMoves(1, 1);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 3 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns34From22()
            {
                var moves = GetMoves(2, 2);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 3 && boardCoordinate.Y == 4));
            }

            [Test]
            public void Returns41From22()
            {
                var moves = GetMoves(2, 2);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 4 && boardCoordinate.Y == 1));
            }

            [Test]
            public void Returns41From33()
            {
                var moves = GetMoves(3, 3);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 4 && boardCoordinate.Y == 1));
            }

            [Test]
            public void Returns43From22()
            {
                var moves = GetMoves(2, 2);
                Assert.IsTrue(moves.Any(boardCoordinate => boardCoordinate.X == 4 && boardCoordinate.Y == 3));
            }
        }
    }
}