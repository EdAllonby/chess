﻿using System.Collections.Generic;
using System.Linq;
using Chess;
using NUnit.Framework;

namespace ChessTests
{
    [TestFixture]
    public class KingTest
    {
        [SetUp]
        public void BeforeEachTest()
        {
            Target = new King();
            movesFrom33 = Target.GetMovesFrom(new BoardCoordinate(3, 3));
            movesFrom11 = Target.GetMovesFrom(new BoardCoordinate(1, 1));
        }

        private King Target { get; set; }
        private IEnumerable<BoardCoordinate> movesFrom33;
        private IEnumerable<BoardCoordinate> movesFrom11;

        [TestFixture]
        public class GetMovesFrom : KingTest
        {
            [Test]
            public void DoesNotReturn00From11()
            {
                Assert.IsFalse(movesFrom11.Any(boardCoordinate => boardCoordinate.X == 0 || boardCoordinate.Y == 0));
            }

            [Test]
            public void Returns12From11()
            {
                Assert.IsTrue(movesFrom11.Any(boardCoordinate => boardCoordinate.X == 1 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns22For11()
            {
                Assert.IsTrue(movesFrom11.Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns23From33()
            {
                Assert.IsTrue(movesFrom33.Any(boardCoordinate => boardCoordinate.X == 2 && boardCoordinate.Y == 3));
            }

            [Test]
            public void Returns32From33()
            {
                Assert.IsTrue(movesFrom33.Any(boardCoordinate => boardCoordinate.X == 3 && boardCoordinate.Y == 2));
            }

            [Test]
            public void Returns34From33()
            {
                Assert.IsTrue(movesFrom33.Any(boardCoordinate => boardCoordinate.X == 3 && boardCoordinate.Y == 4));
            }

            [Test]
            public void DoesNotReturn_35_For_33()
            {
                Assert.IsFalse(movesFrom33.Any(bc=>bc.X == 3 && bc.Y == 5));
            }

            [Test]
            public void HasFiveMovesWhenStartingAtBackEndOfBoard()
            {
                IEnumerable<BoardCoordinate> moves = Target.GetMovesFrom(BoardCoordinate.For(4, 8));

                Assert.AreEqual(5, moves.Count());
            }
        }
    }
}